﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
/*Author: Aishwarya Hajare
 Date: 19/09/2018
 Description: Implemented Stack and ArrayList collection*/
    namespace Capita.UI.ConsoleUI
{
    class CollectionDemo
    {
        public static void Main()
        {
            ArrayList list1 = new ArrayList();
            list1.Add("Aishwarya");
            list1.Add(1996);
            list1.Add(36);
            list1.Add(5.86f);
            foreach (var item in list1)
            {
                Console.WriteLine(item); //Aishwarya 1996 36 5.86
            }
            Console.WriteLine(list1.Count);//4
            Console.WriteLine(list1.Capacity);//4
            Console.WriteLine("\n\n\n");

            ArrayList list2 = new ArrayList();
            list2.Add(10);
            list2.Add(1996);
            list2.Add(36);
            list2.Add(52);
            list2.Sort(); //Sort the give ArrayList
            foreach (var item in list2)
            {
                Console.WriteLine(item); //10 36 52 1996
            }
            
            Stack st = new Stack();
            st.Push("Aishwarya");
            st.Push("Kalyansingh");
            st.Push("Hajare");
            foreach(string str in st)
            {
                Console.WriteLine(str+" "); //Aishwarya Kalyansingh Hajare
            }

            Console.ReadLine();
        }
    }
}
