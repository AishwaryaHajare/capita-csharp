﻿using System;
using System.Linq;
using Capita.Business.CapitaHelper;

namespace Capita.UI.ConsolUI
{
    class ParameterDemo
    {
        public static void Main()
        {
            Utility utility = new Utility();
            int myVal = 20;
            Console.WriteLine("Before calling by value function= {0}", myVal);//20
            utility.ByVal(myVal);//100
            Console.WriteLine("After calling by value function= {0}", myVal);//20

            Console.WriteLine("\nBefore calling by reference function= {0}", myVal);//20
            utility.ByRef(ref myVal);//200
            Console.WriteLine("After calling by reference function= {0}", myVal);//200

            Console.WriteLine("\nBefore calling by Out function= {0}", myVal);//200
            utility.ByOut(out myVal);//400
            Console.WriteLine("After calling by out function= {0}", myVal);//400

            Console.WriteLine("\ncalling params function= {0}", myVal);//300
            myVal = utility.ByParam(10, 20, 30);
            Console.WriteLine("After calling by out function= {0}", myVal);//60

            Console.WriteLine("\ncalling Named function");
            utility.Print(lastName: "Hajare", firstName: "Aishwarya");
            Console.ReadLine();
        }
    }
}

