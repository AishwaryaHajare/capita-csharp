﻿using System;
using Capita.Business.CapitaHelper;
namespace Capita.UI.ConsolUI
{
    class Choice
    {
        public static void Main()
        {
            int num1, num2, result;
            string choice;

            Console.WriteLine("Enter First Number");
            num1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Second Number");
            num2 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter your operation like +,-,*");
            choice = Console.ReadLine();
            switch (choice)
            {
                case "+":
                    result = num1 + num2;
                    Console.WriteLine(result);
                    break;
                case "-":
                    result = num1 - num2;
                    Console.WriteLine(result);
                    break;
                case "*":
                    result = num1 * num2;
                    Console.WriteLine(result);
                    break;
                default:
                    Console.WriteLine("Default case");
                    break;
            }
            Console.ReadLine();
        }
    }
}
