﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Capita.Business.CapitaHelper;
namespace Capita.UI.ConsoleUI
{
    class InterfaceDemo
    {
        public static void Main()
        {
            Human human1 = new Human();
            human1.Breathe();
            human1.Dancing();
            human1.Eating();

            Mammals m1 = human1;
            m1.Breathe();
            
            IAnimals animal;
            IHobby hobby=human1;
            hobby.Breathe();
            hobby.Dancing();
            IMeditation meditat;
            meditat = human1;
            meditat.Breathe();
            Console.ReadLine();
        }
    }
}
