﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capita.Data.Entity
{
    public class Project
    {
        public string ProjectID { get; set; }
        public string ProjectName { get; set; }
        public int ProjectBudget { get; set; }
        public int TeamSize { get; set; }
        public string ProjectStartDate { get; set; }
        public string ProjectEndDate { get; set; }
    }
}
