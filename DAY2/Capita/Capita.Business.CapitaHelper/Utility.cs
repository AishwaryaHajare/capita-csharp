﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capita.Business.CapitaHelper
{
    public class Utility
    {
        /// <summary>
        /// To perform addition 
        /// </summary>
        /// <param name="num1">for 1st user input</param>
        /// <param name="num2">for 2nd user input</param>
        /// <returns>returns addition</returns>
        public int Sum(int num1, int num2)
        {
            return num1+num2;
        }

        public int Substraction(int num1, int num2)
        {
            return num1 - num2;
        }

        public int Multiplication(int num1, int num2)
        {
            return num1 * num2;
        }

        public int Division(int num1, int num2)
        {
            return num1 / num2;
        }
        public void ByVal(int val)
        {
            val = 100;
            Console.WriteLine("In By value  function " + val.ToString());
        }
        public void ByRef(ref int val)
        {
            val = 200;
            Console.WriteLine("In By refrence  function " + val.ToString());
        }
        public void ByOut(out int val)
        {
            val = 400;
            Console.WriteLine("In By out  function " + val.ToString());
        }

        public int ByParam(params int[] data)
        {
            int sum = 0;
            foreach (int item in data)
            {
               sum += item;
            }
            return sum;
        }
        /// <summary>
        /// In this function it is not mandatory to give second parameter. Even if you assign value,it will get override.
        /// </summary>
        /// <param name="values">Aishwarya</param>
        /// <returns></returns>
        public void Print(string firstName, string lastName = "Default Value")
        {
            Console.WriteLine("First Name " + firstName + " Last Name " + lastName);
        }
    }

}
