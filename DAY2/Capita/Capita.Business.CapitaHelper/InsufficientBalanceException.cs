﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capita.Business.CapitaHelper
{
    public class InsufficientBalanceException:ApplicationException
    {
        public InsufficientBalanceException()
        {

        }
        public InsufficientBalanceException(string message ):base(message)
        {

        }
    }
}
