﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* Author: Aishwarya Hajare
 Date: 19/09/2018
 Description: Implemented Use of Generic class*/
namespace Capita.UI.ConsoleUi
{
    class GenericClassEx<T>
    {
        /// <summary>
        /// Swapping Function
        /// </summary>
        /// <param name="num1"></param>
        /// <param name="num2"></param>
        public void Swap(ref T num1,ref T  num2)
        {
            T temp = num1;
            num1 = num2;
            num2 = temp;
        }

        /// <summary>
        /// Addition Function
        /// </summary>
        /// <param name="num1"></param>
        /// <param name="num2"></param>
        public void Addition(ref int num1,ref int num2)
        {
            int result;
            result = num1 + num2;
            Console.WriteLine("Addition of " + num1 + " " + num2+ " is " +result ); //Addition of 10 20 is 30
        }
    }
    class GenericDemo
    {
        public static void Main()
        {
            int num1 = 10;
            int num2 = 20;
            string str1 = "Aishwaray";
            string str2 = "Hajare";
            GenericClassEx<int> genericEx1 = new GenericClassEx<int>();
            genericEx1.Addition(ref num1, ref num2);
            

            GenericClassEx<string> genericEx2 = new GenericClassEx<string>();
            Console.WriteLine("Before Swapping " + str1 + " " + str2); //Before swapping Aishwarya Hajare
            genericEx2.Swap(ref str1, ref str2);
            Console.WriteLine("After Swapping " + str1+ " " + str2); //After Swapping Hajare Aishwrya
            Console.ReadLine();
        }
    }
}
