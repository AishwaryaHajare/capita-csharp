﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Author: Aishwarya Hajare
 Date: 19/09/2018
 Description: Implemented SortedList and Hash Table collection*/
namespace Capita.UI.ConsoleUi
{
    class CollectionDemo2
    {
        static void Main(string[] args)
        {
            //SoertedList Implementation

            SortedList s = new SortedList();

            s.Add("1", "Aishwarya");
            s.Add("2", "Sneha");
            s.Add("3", "Shweta");
            s.Add("4", "Anjali");
            s.Add("5", "Omkar");
            
            if (s.ContainsValue("Akash")) //Check whether Akash is in list or not
            {
                Console.WriteLine("This name is already in the list");
            }
            else 
            {
                s.Add("6", "Akash"); //else add Akash in list
            }

            // get the keys. 
            ICollection key = s.Keys; //Keys is in built variable for accessing keys of SortedList

            foreach (string k in key)
            {
                Console.WriteLine("Key:"+k + ": Value:" + s[k]);
                Console.WriteLine("\n");
            }

            ICollection val = s.Values; //Values is in built variable for accessing values of SortedList

            foreach (string values in val)
            {
                Console.WriteLine(values );
            }

            //Hash Table Implementation
            Hashtable hTable = new Hashtable();

            hTable.Add("1", "Java");
            hTable.Add("2", "C#");
            hTable.Add("3", "Python");
            hTable.Add("4", "C++");
           

            if (hTable.ContainsValue("Cobol"))
            {
                Console.WriteLine("This Programming name is already in the list");
            }
            else
            {
                hTable.Add("5", "Cobol");
            }

            // Get the keys.
            ICollection key1 = hTable.Keys;

            foreach (string k in key1)
            {
                Console.WriteLine(k + ": " + hTable[k]);
            }
            Console.ReadLine();
        }

    }
}
