﻿using System;

namespace Capita.UI.ConsoleUI
{
    class BaseClass
    {
        public BaseClass()
        {
            Console.WriteLine("Base Class default constructor");
        }
        public BaseClass(String str)
        {
            Console.WriteLine("Base Class parameterized constructor " + str);
        }
        public virtual void print()
        {
            
        }
    }
    class ChildClass : BaseClass
    {
        
        public ChildClass()
        {
            Console.WriteLine("Child class default constructor");
        }
        public ChildClass(String str)
        {
            Console.WriteLine("Child Class parameterized constructor " + str);
        }
        public override void print()
        {
            int a, b,c;
            a = 10;
            b = 20;
            c = a + b;
            Console.WriteLine("Addition is:",+c);
        }
    }
    class ConstructorEx
    {
        public static void Main()
        {
            ChildClass C = new ChildClass("ABC");
            ChildClass C1 = new ChildClass("PQR");
            C1.print();
            Console.ReadLine();
        }
    }
}
