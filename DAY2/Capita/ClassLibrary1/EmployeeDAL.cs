﻿using System;
using System.Data;
using System.Data.SqlClient;
using Capita.Data.Common;
using Capita.Data.Entity;

namespace Capita.Data.DAL
{
    public class EmployeeDAL
    {
        
        public int CreateEmployee(Employee emp)
        {
            SqlConnection con = Connection.GetConnection(); //singleton
            SqlCommand command = new SqlCommand("InsertEmployee", con);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter empId = new SqlParameter("@empId", SqlDbType.NVarChar);
            empId.Value = emp.EmployeeID;
            command.Parameters.Add(empId);

            SqlParameter empName = new SqlParameter("@empName", SqlDbType.NVarChar);
            empName.Value = emp.EmployeeName;
            command.Parameters.Add(empName);

            SqlParameter address = new SqlParameter("@address", SqlDbType.NVarChar);
            address.Value = emp.Address;
            command.Parameters.Add(address);

            SqlParameter contact = new SqlParameter("@contactNumber", SqlDbType.NVarChar);
            contact.Value = emp.ContactNumber;
            command.Parameters.Add(contact);

            SqlParameter aadhar = new SqlParameter("@aadharNumber", SqlDbType.NVarChar);
            aadhar.Value = emp.AadharNumber;
            command.Parameters.Add(aadhar);

            SqlParameter age = new SqlParameter("@age", SqlDbType.Int);
            age.Value = emp.Age;
            command.Parameters.Add(age);

            SqlParameter gender = new SqlParameter("@gender", SqlDbType.NVarChar);
            gender.Value = emp.Gender;
            command.Parameters.Add(gender);

            SqlParameter previousOrg = new SqlParameter("@previousOrg", SqlDbType.NVarChar);
            previousOrg.Value = emp.PrevOrg;
            command.Parameters.Add(previousOrg);

            SqlParameter designation = new SqlParameter("@designation", SqlDbType.NVarChar);
            designation.Value = emp.Designation;
            command.Parameters.Add(designation);

            SqlParameter eDuration = new SqlParameter("@duration", SqlDbType.Int);
            eDuration.Value = emp.Duration;
            command.Parameters.Add(eDuration);
            int result = 0;

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            try
            {
                result = command.ExecuteNonQuery();
            }
            catch (SqlException sqlEx)
            {
                //TODO: Write Logging Code
                throw sqlEx;
            }
            return result;

        }

        public int DeleteEmployee(Employee emp)
        {
            SqlConnection con = Connection.GetConnection();
            SqlCommand command = new SqlCommand("DeleteEmployee", con);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter empId = new SqlParameter("@empId", SqlDbType.NVarChar);
            empId.Value = emp.EmployeeID;
            command.Parameters.Add(empId);

            int result = 0;
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            try
            {
                result = command.ExecuteNonQuery();
            }
            catch (SqlException sqlEx)
            {
                //TODO: Write Logging Code
                throw sqlEx;
            }
            return result;
        }
    }
}
