﻿using System;
using System.Data;
using System.Data.SqlClient;
using Capita.Data.Common;
using Capita.Data.Entity;

namespace Capita.Data.DAL
{
    public class ProjectDAL
    {
        public int CreateProject(Project proj)
        {
            SqlConnection con = Connection.GetConnection(); //singleton
            SqlCommand command = new SqlCommand("InsertProject", con);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter pId = new SqlParameter("@ProjectID", SqlDbType.NVarChar);
            pId.Value = proj.ProjectID;
            command.Parameters.Add(pId);

            SqlParameter pName = new SqlParameter("@ProjectName", SqlDbType.NVarChar);
            pName.Value = proj.ProjectName;
            command.Parameters.Add(pName);

            SqlParameter pBudget = new SqlParameter("@ProjectBudget", SqlDbType.Int);
            pBudget.Value = proj.ProjectBudget;
            command.Parameters.Add(pBudget);

            SqlParameter teamSize = new SqlParameter("@TeamSize", SqlDbType.Int);
            teamSize.Value = proj.TeamSize;
            command.Parameters.Add(teamSize);
            int result = 0;
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            try
            {
                result = command.ExecuteNonQuery();
            }
            catch (SqlException sqlEx)
            {
                //TODO: Write Logging Code
                throw sqlEx;
            }
            return result;
        }
        

        public int DeleteProject(Project proj)
        {
            SqlConnection con = Connection.GetConnection();
            SqlCommand command = new SqlCommand("DeleteProject", con);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter pId = new SqlParameter("@ProjectID", SqlDbType.NVarChar);
            pId.Value = proj.ProjectID;
            command.Parameters.Add(pId);
            int result = 0;
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            try
            {
                result = command.ExecuteNonQuery();
            }
            catch (SqlException sqlEx)
            {
                //TODO: Write Logging Code
                throw sqlEx;
            }
            return result;
        }
        public DataSet ReadProject()
        {
            SqlConnection con = Connection.GetConnection();
            SqlCommand sqlCommand = new SqlCommand("GetAllProject", con);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            DataSet dataSet;
            
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                dataSet = new DataSet();
                adapter.Fill(dataSet);
                
            }
            catch (SqlException sqlEx)
            {
                //TODO: Write Logging Code
                throw sqlEx;
            }
            return dataSet;
        }

        public int UpdateProject(Project proj)
        {
            int result;
            SqlConnection sqlConnection = Connection.GetConnection();
            SqlCommand command = new SqlCommand("UpdateProject",sqlConnection);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter pId = new SqlParameter("@ProjectID", SqlDbType.NVarChar);
            pId.Value = proj.ProjectID;
            command.Parameters.Add(pId);

            SqlParameter  pName = new SqlParameter("@ProjectName", SqlDbType.NVarChar);
            pName.Value = proj.ProjectName;
            command.Parameters.Add(pName);

            SqlParameter pBudget = new SqlParameter("@ProjectBudget", SqlDbType.Int);
            pBudget.Value = proj.ProjectBudget;
            command.Parameters.Add(pBudget);

            SqlParameter teamSize = new SqlParameter("@TeamSize", SqlDbType.Int);
            teamSize.Value = proj.TeamSize;
            command.Parameters.Add(teamSize);

            if (sqlConnection.State == ConnectionState.Closed)
            {
                sqlConnection.Open();
            }
            try
            {
                result = command.ExecuteNonQuery();
            }
            catch (SqlException sqlEx)
            {
                //TODO: Write Logging Code
                throw sqlEx;
            }
            return result;
        }

        public DataSet FindProjectByID(Project proj)
        {
            SqlConnection con = Connection.GetConnection();
            SqlCommand sqlCommand = new SqlCommand("GetProjectByProjectId", con);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter pId = new SqlParameter("@ProjectID", SqlDbType.NVarChar);
            pId.Value = proj.ProjectID;
            sqlCommand.Parameters.Add(pId);

            DataSet dataSet;

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                dataSet = new DataSet();
                adapter.Fill(dataSet);

            }
            catch (SqlException sqlEx)
            {
                //TODO: Write Logging Code
                throw sqlEx;
            }
            return dataSet;
        }
    }
}       
