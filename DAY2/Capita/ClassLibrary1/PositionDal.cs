﻿using System;
using System.Data;
using System.Data.SqlClient;
using Capita.Data.Common;
using Capita.Data.Entity;
namespace Capita.Data.DAL
{
    public class PositionDal
    {
        public int CreateEmpPosition(Position pos)
        {
            SqlConnection con = Connection.GetConnection(); //singleton
            SqlCommand comm = new SqlCommand("InsertPosition", con);
            comm.CommandType = CommandType.StoredProcedure;
            SqlParameter posid = new SqlParameter("@positionId", SqlDbType.NVarChar);
            posid.Value = pos.PositionId;
            comm.Parameters.Add(posid);

            SqlParameter desc = new SqlParameter("@description", SqlDbType.NVarChar);
            desc.Value = pos.Description;
            comm.Parameters.Add(desc);

            SqlParameter role = new SqlParameter("@role", SqlDbType.NVarChar);
            role.Value = pos.Role;
            comm.Parameters.Add(role);

            SqlParameter band = new SqlParameter("@band", SqlDbType.NVarChar);
            band.Value = pos.Band;
            comm.Parameters.Add(band);

            SqlParameter desig = new SqlParameter("@designation", SqlDbType.NVarChar);
            desig.Value = pos.Designation;
            comm.Parameters.Add(desig);

            SqlParameter skill = new SqlParameter("@skill", SqlDbType.NVarChar);
            skill.Value = pos.Skill;
            comm.Parameters.Add(skill);

            int result = 0;
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            try
            {
                result = comm.ExecuteNonQuery();
            }
            catch (SqlException sqlEx)
            {
                //TODO: Write Logging Code
                throw sqlEx;
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public int UpdateEmpPosition(Position pos)
        {
            SqlConnection con = Connection.GetConnection(); //singleton
            SqlCommand comm = new SqlCommand("UpdatePosition", con);
            comm.CommandType = CommandType.StoredProcedure;
            SqlParameter posid = new SqlParameter("@positionId", SqlDbType.NVarChar);
            posid.Value = pos.PositionId;
            comm.Parameters.Add(posid);

            SqlParameter desc = new SqlParameter("@description", SqlDbType.NVarChar);
            desc.Value = pos.Description;
            comm.Parameters.Add(desc);

            SqlParameter role = new SqlParameter("@role", SqlDbType.NVarChar);
            role.Value = pos.Role;
            comm.Parameters.Add(role);

            SqlParameter band = new SqlParameter("@band", SqlDbType.NVarChar);
            band.Value = pos.Band;
            comm.Parameters.Add(band);

            SqlParameter desig = new SqlParameter("@designation", SqlDbType.NVarChar);
            desig.Value = pos.Designation;
            comm.Parameters.Add(desig);

            SqlParameter skill = new SqlParameter("@skill", SqlDbType.NVarChar);
            skill.Value = pos.Skill;
            comm.Parameters.Add(skill);

            int result = 0;
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            try
            {
                result = comm.ExecuteNonQuery();
            }
            catch (SqlException sqlEx)
            {
                //TODO: Write Logging Code
                throw sqlEx;
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public DataSet ReadEmpPosition()
        {
            SqlConnection con = Connection.GetConnection();
            SqlCommand sqlCommand = new SqlCommand("GetAllPosition", con);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            DataSet dataSet;

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                dataSet = new DataSet();
                adapter.Fill(dataSet);

            }
            catch (SqlException sqlEx)
            {
                //TODO: Write Logging Code
                throw sqlEx;
            }
            return dataSet;
        }


        public int DeleteEmpPosition(Position pos)
        {
            SqlConnection con = Connection.GetConnection(); //singleton

            SqlCommand comm = new SqlCommand("DeletePosition", con);
            comm.CommandType = CommandType.StoredProcedure;
            SqlParameter posid = new SqlParameter("@positionId", SqlDbType.NVarChar);
            posid.Value = pos.PositionId;
            comm.Parameters.Add(posid);

            int result = 0;
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            try
            {
                result = comm.ExecuteNonQuery();
            }
            catch (SqlException sqlEx)
            {
                //TODO: Write Logging Code
                throw sqlEx;
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public DataSet ReadEmpPositionByID(Position position)
        {
            SqlConnection con = Connection.GetConnection();
            SqlCommand sqlCommand = new SqlCommand("GetPositionById", con);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter pId = new SqlParameter("@PositionId", SqlDbType.NVarChar);
            pId.Value = position.PositionId;
            sqlCommand.Parameters.Add(pId);

            DataSet dataSet;

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                dataSet = new DataSet();
                adapter.Fill(dataSet);

            }
            catch (SqlException sqlEx)
            {
                //TODO: Write Logging Code
                throw sqlEx;
            }
            return dataSet;
        }
    }
}