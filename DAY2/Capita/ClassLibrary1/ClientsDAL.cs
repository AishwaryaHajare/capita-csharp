﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Capita.Data.Common;
using Capita.Data.DAL;
using Capita.Data.Entity;

namespace Capita.Data.DAL
{
    public class ClientsDAL
    {
        


        public int CreateClient(Client client)
        {
            SqlConnection con = Connection.GetConnection(); //singleton
            SqlCommand command = new SqlCommand("InsertClient", con);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter clientId = new SqlParameter("@ClientID", SqlDbType.NVarChar);
            clientId.Value = client.ClientID;
            command.Parameters.Add(clientId);

            SqlParameter clientName = new SqlParameter("@ClientName", SqlDbType.NVarChar);
            clientName.Value = client.ClientName;
            command.Parameters.Add(clientName);

            SqlParameter password = new SqlParameter("@Password", SqlDbType.NVarChar);
            password.Value = client.Password;
            command.Parameters.Add(password);

            SqlParameter contact = new SqlParameter("@Contact", SqlDbType.NVarChar);
            contact.Value = client.Contact;
            command.Parameters.Add(contact);

            SqlParameter email = new SqlParameter("@Email", SqlDbType.NVarChar);
            email.Value = client.Email;
            command.Parameters.Add(email);
            int result = 0;
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            try
            {
                result = command.ExecuteNonQuery();
            }
            catch (SqlException sqlEx)
            {
                //TODO: Write Logging Code
                throw sqlEx;
            }
            return result;
        }

        public DataSet FindClientByID(Client client)
        {
            SqlConnection con = Connection.GetConnection();
            SqlCommand sqlCommand = new SqlCommand("GetClientByClientId", con);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter clientId = new SqlParameter("@ClientID", SqlDbType.NVarChar);
            clientId.Value = client.ClientID;
            sqlCommand.Parameters.Add(clientId);

            DataSet dataSet;

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                dataSet = new DataSet();
                adapter.Fill(dataSet);

            }
            catch (SqlException sqlEx)
            {
                //TODO: Write Logging Code
                throw sqlEx;
            }
            return dataSet;
        }

        public DataSet ReadProject()
        {
            SqlConnection con = Connection.GetConnection();
            SqlCommand sqlCommand = new SqlCommand("GetAllClient", con);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            DataSet dataSet;

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                dataSet = new DataSet();
                adapter.Fill(dataSet);

            }
            catch (SqlException sqlEx)
            {
                //TODO: Write Logging Code
                throw sqlEx;
            }
            return dataSet;
        }

        public int UpdateClient(Client client)
        {
            SqlConnection con = Connection.GetConnection(); //singleton
            SqlCommand command = new SqlCommand("UpdateClient", con);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter clientId = new SqlParameter("@ClientID", SqlDbType.NVarChar);
            clientId.Value = client.ClientID;
            command.Parameters.Add(clientId);

            SqlParameter clientName = new SqlParameter("@ClientName", SqlDbType.NVarChar);
            clientName.Value = client.ClientName;
            command.Parameters.Add(clientName);

            SqlParameter password = new SqlParameter("@Password", SqlDbType.NVarChar);
            password.Value = client.Password;
            command.Parameters.Add(password);

            SqlParameter contact = new SqlParameter("@Contact", SqlDbType.NVarChar);
            contact.Value = client.Contact;
            command.Parameters.Add(contact);

            SqlParameter email = new SqlParameter("@Email", SqlDbType.NVarChar);
            email.Value = client.Email;
            command.Parameters.Add(email);
            int result = 0;
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            try
            {
                result = command.ExecuteNonQuery();
            }
            catch (SqlException sqlEx)
            {
                //TODO: Write Logging Code
                throw sqlEx;
            }
            return result;
        }

        public int DeleteClient(Client client)
        {
            SqlConnection con = Connection.GetConnection();
            SqlCommand command = new SqlCommand("DeleteClient", con);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter clientId = new SqlParameter("@ClientID", SqlDbType.NVarChar);
            clientId.Value = client.ClientID;
            command.Parameters.Add(clientId);
            int result = 0;
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            try
            {
                result = command.ExecuteNonQuery();
            }
            catch (SqlException sqlEx)
            {
                //TODO: Write Logging Code
                throw sqlEx;
            }
            return result;
        }
    }
}
