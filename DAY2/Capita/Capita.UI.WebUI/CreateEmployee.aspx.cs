﻿using System;
using Capita.Controllers.Controller;

namespace Capita.UI.WebUI
{
    public partial class CreateEmployee : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            EmployeeController empController = new EmployeeController();
            string msg = empController.CreateEmployee(txtEmpID.Text,txtEmpName.Text,txtEmpAdress.Text,txtEmpContact.Text,txtEmpAadhar.Text,
                int.Parse(txtEmpAge.Text),txtEmpGender.Text,txtEmpPrevOrg.Text,txtEmpDesignation.Text,int.Parse(txtEmpDuration.Text));
            lblMsg.Text = msg;
        }
    }
}