﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Capita.Master" AutoEventWireup="true" CodeBehind="ReadEmployee.aspx.cs" Inherits="Capita.UI.WebUI.ReadEmployee" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Show" />
    <br />
    <asp:GridView ID="gvReadEmployee" runat="server">
    </asp:GridView>
    <br />
    <asp:TextBox ID="txtEmpID" runat="server"></asp:TextBox>
    <br />
    <br />
    <asp:Button ID="Button2" runat="server" Text="Find Employee By ID" />
    <br />
</asp:Content>
