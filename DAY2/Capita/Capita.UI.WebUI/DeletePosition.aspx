﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Capita.Master" AutoEventWireup="true" CodeBehind="DeletePosition.aspx.cs" Inherits="Capita.UI.WebUI.DeletePosition" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style4 {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="auto-style4">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="PositionID"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtPositionID" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Delete" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="lblMsg" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
