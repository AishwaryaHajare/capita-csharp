﻿using System;
using System.Data;
using Capita.Controller.ProjectController;
namespace Capita.UI.WebUI
{
    public partial class ReadClient : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ClientController clientController = new ClientController();
            DataSet dataSet = clientController.ReadClient();
            gvReadClient.DataSource = dataSet.Tables[0];
            gvReadClient.DataBind();

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            ClientController clientController = new ClientController();
            DataSet dataSet = clientController.FindClientByID(txtClientID.Text);
            gvReadClient.DataSource = dataSet.Tables[0];
            gvReadClient.DataBind();
        }
    }
}