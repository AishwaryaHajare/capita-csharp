﻿using System;
using Capita.Controller.ProjectController;
namespace Capita.UI.WebUI
{
    public partial class DeleteClient : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ClientController clientController = new ClientController();
            lblMsg.Text = clientController.DeleteClient(txtClientID.Text);

        }
    }
}