﻿using System;
using Capita.Controllers.Controller;
namespace Capita.UI.WebUI
{
    public partial class DeleteEmployee1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            EmployeeController employeeController = new EmployeeController();
            string msg = employeeController.DeleteEmployee(txtEmpID.Text);
            lblMsg.Text = msg;
        }
    }
}