﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Capita.Controllers.Controller;
namespace Capita.UI.WebUI
{
    public partial class UpdateProject : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            ProjectController projectController = new ProjectController();
            string msg = projectController.UpdateProject(txtProjectID.Text, txtProjectName.Text, int.Parse(txtProjectBudget.Text), int.Parse(txtTeamSize.Text));
            lblMsg.Text = msg;
        }
    }
}