﻿using System;
using Capita.Controller.EmployeePosition;
namespace Capita.UI.WebUI
{
    public partial class DeletePosition : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            PositionController positionController = new PositionController();
            string msg = positionController.DeleteEmpPosition(txtPositionID.Text);
            lblMsg.Text = msg;
        }
    }
}