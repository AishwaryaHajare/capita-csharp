﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Capita.Master" AutoEventWireup="true" CodeBehind="ReadProject.aspx.cs" Inherits="Capita.UI.WebUI.ReadProject" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Button ID="btnReadProject" runat="server" Text="Read" OnClick="btnReadProject_Click" />
    <asp:GridView ID="gvReadProject" runat="server" OnSelectedIndexChanged="gvReadProject_SelectedIndexChanged">
    </asp:GridView>
    <asp:TextBox ID="FindProByID" runat="server"></asp:TextBox>
    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Find Project By ID" />
</asp:Content>
