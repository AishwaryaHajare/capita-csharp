﻿using System;
using System.Data;
using Capita.Controller.EmployeePosition;
namespace Capita.UI.WebUI
{
    public partial class ReadPosition : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            PositionController positionController = new PositionController();
            DataSet dataSet = positionController.ReadEmpPosition();
            gvShow.DataSource = dataSet.Tables[0];
            gvShow.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            PositionController positionController = new PositionController();
            DataSet dataSet = positionController.ReadEmpPositionByID(txtFindByID.Text);
            gvShow.DataSource = dataSet.Tables[0];
            gvShow.DataBind();
        }
    }
}