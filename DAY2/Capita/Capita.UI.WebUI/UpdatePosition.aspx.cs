﻿using System;
using Capita.Controller.EmployeePosition;
namespace Capita.UI.WebUI
{
    public partial class UpdatePosition : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            PositionController positionController = new PositionController();
            string msg = positionController.UpdateEmpPosition(txtPositionID.Text, txtDescription.Text, txtRole.Text, txtBand.Text, txtDesignation.Text, txtSkill.Text);
            lblMsg.Text = msg;
        }
    }
}