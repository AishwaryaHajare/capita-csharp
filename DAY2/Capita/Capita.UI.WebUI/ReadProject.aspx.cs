﻿using System;
using Capita.Controllers.Controller;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Capita.UI.WebUI
{
    public partial class ReadProject : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnReadProject_Click(object sender, EventArgs e)
        {
            ProjectController projectController = new ProjectController();
            DataSet dataSet = projectController.ReadProject();
            gvReadProject.DataSource = dataSet.Tables[0];
            gvReadProject.DataBind();
        }

        protected void gvReadProject_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ProjectController projectController = new ProjectController();
            DataSet dataSet = projectController.FindProjectByID(FindProByID.Text);
            gvReadProject.DataSource = dataSet.Tables[0];
            gvReadProject.DataBind();
        }
    }
}