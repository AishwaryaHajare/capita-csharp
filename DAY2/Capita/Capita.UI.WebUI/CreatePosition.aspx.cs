﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Capita.Controller.EmployeePosition;
using Capita.Data.Entity;
namespace Capita.UI.WebUI
{
    public partial class CreatePosition : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCreatePosition_Click(object sender, EventArgs e)
        {
            PositionController positionController = new PositionController();
            string msg = positionController.CreateEmpPosition(txtPositionID.Text, txtDeScription.Text, txtRole.Text,txtBand.Text,txtDesignation.Text,txtSkill.Text);
            lblMsg.Text = msg;
        }
    }
}