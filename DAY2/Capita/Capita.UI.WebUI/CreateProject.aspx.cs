﻿using System;
using Capita.Controller.ProjectController;
using Capita.Controllers.Controller;

namespace Capita.UI.WebUI
{
    public partial class CreateProject : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ProjectController projectController = new ProjectController();
            string msg = projectController.CreateProject(txtProjectID.Text, txtProjectName.Text, int.Parse(txtProjectBudget.Text), int.Parse(txtTeamSize.Text));
            lblMsg.Text = msg;
        }
    }
}