﻿using System;
using System.Data;
using Capita.Controllers.Controller;

namespace Capita.UI.WebUI
{
    public partial class ReadEmployee : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            EmployeeController empController = new EmployeeController();
            DataSet dataSet = empController.ReadEmployee();
            gvReadEmployee.DataSource = dataSet.Tables[0];
            gvReadEmployee.DataBind();
        }
    }
}