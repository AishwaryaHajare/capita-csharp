﻿using System;
using Capita.Controller.ProjectController;
namespace Capita.UI.WebUI
{
    public partial class CreateClient : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ClientController clientController = new ClientController();
            string msg = clientController.CreateClient(txtClientID.Text,txtClientName.Text,txtClientPassword.Text,txtClientContact.Text,txtClientEmail.Text);
            lblMsg.Text = msg;
        }
    }
}