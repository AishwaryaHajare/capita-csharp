﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Capita.Master" AutoEventWireup="true" CodeBehind="DeleteProject.aspx.cs" Inherits="Capita.UI.WebUI.DeleteProject" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style8 {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="auto-style8">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Project ID"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtProjectID" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="btnDeleteProject" runat="server" Text="Delete" OnClick="btnDeleteProject_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="lblMsg" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
