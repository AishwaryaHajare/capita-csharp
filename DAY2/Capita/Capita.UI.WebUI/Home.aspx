﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Capita.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Capita.UI.WebUI.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
   <asp:Menu ID="Menu1" runat="server" Orientation="Horizontal"> 
                                <Items>
                                    <asp:MenuItem Text="Project" Value="Project" >
                                        <asp:MenuItem Text="Create Project" NavigateUrl="~/CreateProject.aspx" Value="Create Project"></asp:MenuItem>
                                        <asp:MenuItem Text="Delete Project" NavigateUrl ="~/DeleteEmployee.aspx" Value="Delete Project"></asp:MenuItem>
                                        <asp:MenuItem Text="Read Project" NavigateUrl="~/ReadProject.aspx"  Value="Read Project"></asp:MenuItem>
                                        <asp:MenuItem Text="Update Project" NavigateUrl="~/UpdateProject.aspx"  Value="Update Project"></asp:MenuItem>
                                    </asp:MenuItem>
                    
                                    <asp:MenuItem Text="Employee" Value="Employee" >
                                        <asp:MenuItem Text="Create Employee" NavigateUrl="~/CreateEmployee.aspx" Value="Create Employee"></asp:MenuItem>
                                        <asp:MenuItem Text="Delete Employee" NavigateUrl ="~/DeleteEmployee.aspx" Value="Delete Employee"></asp:MenuItem>
                                        <asp:MenuItem Text="Read EMployee" NavigateUrl="~/ReadProject.aspx"  Value="Read Employee"></asp:MenuItem>
                                    </asp:MenuItem>
                    
                                    <asp:MenuItem Text="Position" Value="Position" >
                                        <asp:MenuItem Text="Create Position" NavigateUrl="~/CreatePosition.aspx" Value="Create Position"></asp:MenuItem>
                                        <asp:MenuItem Text="Delete Position" NavigateUrl ="~/DeletePosition.aspx" Value="Delete Position"></asp:MenuItem>
                                        <asp:MenuItem Text="Read Position" NavigateUrl="~/ReadPosition.aspx"  Value="Read Position"></asp:MenuItem>
                                        <asp:MenuItem Text="Update Position" NavigateUrl="~/UpdatePosition.aspx"  Value="Update Position"></asp:MenuItem>
                                    </asp:MenuItem>

                                    <asp:MenuItem Text="Client" Value="Client" >
                                        <asp:MenuItem Text="Create Client" NavigateUrl="~/CreateClient.aspx" Value="Create Client"></asp:MenuItem>
                                        <asp:MenuItem Text="Delete Client" NavigateUrl ="~/DeleteClient.aspx" Value="Delete Client"></asp:MenuItem>
                                        <asp:MenuItem Text="Read Client" NavigateUrl="~/ReadClient.aspx"  Value="Read Client"></asp:MenuItem>
                                        <asp:MenuItem Text="Update Client" NavigateUrl="~/UpdateClient.aspx"  Value="Update Client"></asp:MenuItem>
                                    </asp:MenuItem>
                                </Items>
                        </asp:Menu>
    <br />
    <br />
    <br />
    
    <asp:Image ID="Image2" runat="server" Height="630px" ImageUrl="~/Img/Capita1.jpg" />
</asp:Content>
