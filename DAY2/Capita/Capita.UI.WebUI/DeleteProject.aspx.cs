﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Capita.Controllers.Controller;

namespace Capita.UI.WebUI
{
    public partial class DeleteProject : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnDeleteProject_Click(object sender, EventArgs e)
        {
            ProjectController projectController = new ProjectController();
            lblMsg.Text = projectController.DeleteProject(txtProjectID.Text);
            
        }
    }
}